#include <Arduino.h>

hw_timer_t *timer = NULL; // create a hardware timer
#define TIMER_PIN 23

/****** Global Variables ******/
volatile byte pin_state = LOW;

/******************************** 
 * Function Declaration
 ********************************/
void IRAM_ATTR onTimer();

void setup() {
  Serial.begin(115200);
   /*******  Timer *********/
  timer = timerBegin(0, 80, true);

  /* Attach onTimer function to our timer */
  timerAttachInterrupt(timer, &onTimer, true);

  /* Set alarm to call onTimer function every second 1 tick is 1us
  => 1 second is 1000000us */
  /* Repeat the alarm (third parameter) */
  timerAlarmWrite(timer, 3, true);
}

void loop() {
  // put your main code here, to run repeatedly:
}

/******************************** 
 * Function Definitions
 ********************************/
void IRAM_ATTR onTimer()
{
  pin_state = !pin_state;
  digitalWrite(TIMER_PIN, pin_state);
}

